package mova.game.dice;

import java.util.concurrent.ThreadLocalRandom;

public class RollableTestUtils {

    static int getRandomFaces() {
        return _getRandomInt(100);
    }

    static int getRandomNumber() {
        return _getRandomInt(20);
    }

    private static int _getRandomInt(int bound) {
        return ThreadLocalRandom.current().nextInt(bound) + 1;
    }
}
