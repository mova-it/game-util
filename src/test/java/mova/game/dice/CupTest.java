package mova.game.dice;

import mova.game.dice.Cup.Roll;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.function.ToIntFunction;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class CupTest {

    @Test
    void impossibleCup() {
        assertThrows(ImpossibleCupException.class, () -> new Cup(null));
        assertThrows(ImpossibleCupException.class, () -> new Cup(new Rollable[] {null}));
    }

    @Test
    void emptyCup() {
        Cup cup = new Cup(new Rollable[0]);
        assertEquals(0, cup.size());

        Roll roll = cup.roll();
        assertNull(roll);

        roll = cup.roll(rollable -> rollable.roll(true));
        assertNull(roll);

        roll = cup.roll((rollable, i) -> rollable.roll(i == 0));
        assertNull(roll);
    }

    @Test
    void nDxCup() {
        int number = RollableTestUtils.getRandomNumber();

        Rollable[] rollables = new Rollable[number];
        for (int i = 0; i < rollables.length; i++) {
            int faces = RollableTestUtils.getRandomFaces();
            rollables[i] = Die.getRollable(faces);
        }

        Cup cup = new Cup(rollables);
        assertEquals(number, cup.size());
        assertArrayEquals(rollables, cup.getContent());

        Roll roll = cup.roll();
        _validate(cup, roll);

        roll = cup.roll((ToIntFunction<Rollable>) Rollable::roll);
        _validate(cup, roll);

        roll = cup.roll((rollable, i) -> rollable.roll());
        _validate(cup, roll);
    }

    private void _validate(Cup cup, Roll roll) {
        assertNotNull(roll);
        assertNotNull(roll.getResults());
        assertEquals(cup.size(), roll.size());

        int i = 0;
        for (int result : roll) {
            Rollable rollable = cup.getContent()[i++];
            assertTrue(result > 0);
            assertTrue(result <= rollable.getFaces());
        }

        int[] results = roll.getResults();
        assertEquals(IntStream.of(results).sum(), roll.sum());

        for (int j = 0; j < results.length; j++) {
            int[] expectedResults = Arrays.copyOf(results, results.length);
            Arrays.sort(expectedResults);
            expectedResults = ArrayUtils.subarray(expectedResults, expectedResults.length - j, expectedResults.length);

            int[] highestResults = new Roll(roll).retains(j, Roll.RetainMode.HIGHEST).getResults();
            assertArrayEquals(expectedResults, highestResults, "Expected = " + Arrays.toString(expectedResults) + "; Actual = " + Arrays.toString(highestResults));

            highestResults = new Roll(roll).retains(j).getResults();
            assertArrayEquals(expectedResults, highestResults, "Expected = " + Arrays.toString(expectedResults) + "; Actual = " + Arrays.toString(highestResults));

            expectedResults = Arrays.copyOf(results, results.length);
            Arrays.sort(expectedResults);
            expectedResults = ArrayUtils.subarray(expectedResults, 0, j);

            int[] lowestResults = new Roll(roll).retains(j, Roll.RetainMode.LOWEST).getResults();
            assertArrayEquals(expectedResults, lowestResults, "Expected = " + Arrays.toString(expectedResults) + "; Actual = " + Arrays.toString(lowestResults));
        }
    }
}
