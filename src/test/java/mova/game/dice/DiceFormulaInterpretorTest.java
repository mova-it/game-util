package mova.game.dice;

import mova.lang.MathUtils;
import mova.util.test.AssertUtils;
import mova.util.RandomUtils;
import mova.util.StringUtils;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class DiceFormulaInterpretorTest {

    private static final String COUCOU_STRING = "coucou";

    private final Logger logger = Logger.getLogger(DiceFormulaInterpretorTest.class.getName());

    @Test
    void impossibleFormula() {
        assertThrows(NullPointerException.class, () -> new DiceFormulaInterpretor(null));
        assertThrows(NullPointerException.class, () -> DiceFormulaInterpretor.resolve(null));
        assertThrows(IllegalArgumentException.class, () -> new DiceFormulaInterpretor(StringUtils.EMPTY));
        assertThrows(IllegalArgumentException.class, () -> DiceFormulaInterpretor.resolve(StringUtils.EMPTY));
        assertThrows(IllegalArgumentException.class, () -> new DiceFormulaInterpretor(COUCOU_STRING));
        assertThrows(IllegalArgumentException.class, () -> DiceFormulaInterpretor.resolve(COUCOU_STRING));
    }

    @Test
    void zeroFormula() {
        int number = 0;
        _numberFormula(number);
    }

    @Test
    void positiveNumberFormula() {
        int number = RandomUtils.randInt(1000);
        _numberFormula(number);
    }

    @Test
    void negativeNumberFormula() {
        int number = RandomUtils.randInt(1000) - 1000;
        _numberFormula(number);
    }

    private void _numberFormula(int number) {
        String formula = String.valueOf(number);
        logger.log(Level.INFO, "Test de la formule \"{0}\"", formula);

        assertEquals(number, DiceFormulaInterpretor.resolve(formula));
        assertEquals(number, new DiceFormulaInterpretor(formula).interpret());
    }

    @Test
    void formulaWithZero() {
        try {
            final String[] signs = new String[]{"-", "+"};

            int number = RandomUtils.randInt(100) + 1;
            String dice = (RandomUtils.randInt(100) + 1) + Die.DIE_NAME_PREFIX + (RandomUtils.randInt(100) + 1);

            for (String prefixSign : new String[]{"", "-", "+"}) {
                for (String sign : signs) {
                    for (String suffixSign : new String[]{"", "-", "+"}) {
                        String formula = prefixSign + number + sign + "0" + (suffixSign.isEmpty() ? "" : suffixSign + number);
                        DiceFormulaInterpretor.resolve(formula);

                        formula = prefixSign + dice + sign + "0" + (suffixSign.isEmpty() ? "" : suffixSign + dice);
                        DiceFormulaInterpretor.resolve(formula);
                    }

                    for (String suffixSign : signs) {
                        String formula = prefixSign + number + sign + "0" + suffixSign + dice;
                        DiceFormulaInterpretor.resolve(formula);

                        formula = prefixSign + dice + sign + "0" + suffixSign + number;
                        DiceFormulaInterpretor.resolve(formula);
                    }
                }

                for (String sign : signs) {
                    String formula = prefixSign + "0" + sign + number;
                    DiceFormulaInterpretor.resolve(formula);

                    formula = prefixSign + "0" + sign + dice;
                    DiceFormulaInterpretor.resolve(formula);
                }
            }
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void sumPositivesNumbersFormula() {
        int[] numbers = new int[RandomUtils.randInt(100) + 1];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = RandomUtils.randInt(1000);
        }

        _sumNumbersFormula(numbers);
    }

    @Test
    void sumAnyNumbersFormula() {
        int[] numbers = new int[RandomUtils.randInt(100) + 1];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = RandomUtils.randInt(2000) - 1000;
        }

        _sumNumbersFormula(numbers);
    }

    private void _sumNumbersFormula(int[] numbers) {
        int sum = Arrays.stream(numbers).sum();
        String formula = Arrays.stream(numbers).mapToObj(MathUtils::toSignedString).collect(Collectors.joining());
        logger.log(Level.INFO, "Test de la formule \"{0}\"", formula);

        assertEquals(sum, DiceFormulaInterpretor.resolve(formula));
        assertEquals(sum, new DiceFormulaInterpretor(formula).interpret());
    }

    @Test
    void impossibleDiceFormula() {
        assertThrows(IllegalArgumentException.class, () -> new DiceFormulaInterpretor("D0"));
        assertThrows(IllegalArgumentException.class, () -> DiceFormulaInterpretor.resolve("D0"));
        assertThrows(IllegalArgumentException.class, () -> new DiceFormulaInterpretor("D-23"));
        assertThrows(IllegalArgumentException.class, () -> DiceFormulaInterpretor.resolve("D-23"));
    }

    @Test
    void simpleDieFormula() {
        int faces = RandomUtils.randInt(1000) + 1;
        String formula = Die.DIE_NAME_PREFIX + faces;
        logger.log(Level.INFO, "Test de la formule \"{0}\"", formula);

        int result = DiceFormulaInterpretor.resolve(formula);
        assertTrue(result >= 1, result + " < 1");
        assertTrue(result <= faces, result + " > " + faces);

        result = new DiceFormulaInterpretor(formula).interpret();
        assertTrue(result >= 1, result + " < 1");
        assertTrue(result <= faces, result + " > " + faces);
    }

    @Test
    void simpleDiceFormula() {
        int mod = _randomMod();
        int factor = mod*(RandomUtils.randInt(100) + 1);
        int faces = RandomUtils.randInt(1000) + 1;
        String formula = factor + Die.DIE_NAME_PREFIX + faces;
        logger.log(Level.INFO, "Test de la formule \"{0}\"", formula);

        int min = factor < 0 ? factor*faces : factor;
        int max = factor > 0 ? factor*faces : factor;

        logger.log(Level.INFO, "MIN = {0}; MAX = {1}", new Object[] {min, max});

        int result = DiceFormulaInterpretor.resolve(formula);
        AssertUtils.assertInRangeClosed(min, max, result);

        result = new DiceFormulaInterpretor(formula).interpret();
        AssertUtils.assertInRangeClosed(min, max, result);
    }

    @Test
    void multipleDiceFormula() {
        int[] factors = new int[RandomUtils.randInt(100) + 1];
        int[] faces = new int[factors.length];

        for (int i = 0; i < factors.length; i++) {
            int mod = _randomMod();
            factors[i] = mod*(RandomUtils.randInt(100) + 1);
            faces[i] = RandomUtils.randInt(1000) + 1;
        }

        List<Integer> indexes = IntStream.range(0, factors.length).boxed().collect(Collectors.toList());
        Collections.shuffle(indexes);

        String formula = indexes.stream().map(i -> MathUtils.toSignedString(factors[i]) + Die.DIE_NAME_PREFIX + faces[i]).collect(Collectors.joining());
        logger.log(Level.INFO, "Test de la formule \"{0}\"", formula);

        int min = _minimize(factors, faces);
        int max = _maximize(factors, faces);

        logger.log(Level.INFO, "MIN = {0}; MAX = {1}", new Object[] {min, max});

        int result = DiceFormulaInterpretor.resolve(formula);
        AssertUtils.assertInRangeClosed(min, max, result);

        result = new DiceFormulaInterpretor(formula).interpret();
        AssertUtils.assertInRangeClosed(min, max, result);
    }

    @Test
    void complexFormula() {
        int[] numbers = new int[RandomUtils.randInt(100) + 1];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = (RandomUtils.randInt(2000) - 1000);
        }

        int[] factors = new int[RandomUtils.randInt(100) + 1];
        int[] faces = new int[factors.length];

        for (int i = 0; i < factors.length; i++) {
            int mod = _randomMod();
            factors[i] = mod*(RandomUtils.randInt(100) + 1);
            faces[i] = RandomUtils.randInt(1000) + 1;
        }

        List<Integer> indexes = IntStream.range(0, numbers.length + factors.length).boxed().collect(Collectors.toList());
        Collections.shuffle(indexes);

        String formula = indexes.stream().map(i -> i < numbers.length ? MathUtils.toSignedString(numbers[i]) : MathUtils.toSignedString(factors[i - numbers.length]) + Die.DIE_NAME_PREFIX + faces[i - numbers.length]).collect(Collectors.joining());
        logger.log(Level.INFO, "Test de la formule \"{0}\"", formula);

        int sum = IntStream.of(numbers).sum();
        int min = sum + _minimize(factors, faces);
        int max = sum + _maximize(factors, faces);

        logger.log(Level.INFO, "MIN = {0}; MAX = {1}", new Object[]{min, max});

        int result = DiceFormulaInterpretor.resolve(formula);
        AssertUtils.assertInRangeClosed(min, max, result);

        result = new DiceFormulaInterpretor(formula).interpret();
        AssertUtils.assertInRangeClosed(min, max, result);
    }

    private static int _randomMod() {
        return RandomUtils.randInt(2)%2 == 0 ? -1 : 1;
    }

    private static int _minimize(int[] factors, int[] faces) {
        return _sumarize(factors, faces, factor -> factor < 0);
    }

    private static int _maximize(int[] factors, int[] faces) {
        return _sumarize(factors, faces, factor -> factor > 0);
    }

    private static int _sumarize(int[] factors, int[] faces, Predicate<Integer> predicate) {
        return IntStream.range(0, factors.length).map(i -> predicate.test(factors[i]) ? factors[i]*faces[i] : factors[i]).sum();
    }

}
