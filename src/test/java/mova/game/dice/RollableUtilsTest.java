package mova.game.dice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RollableUtilsTest {

    @Test
    void impossibleRollableCup() {
        int faces = RollableTestUtils.getRandomFaces();
        int number = RollableTestUtils.getRandomNumber();
        assertThrows(ImpossibleRollableException.class, () -> RollableUtils.getRollables(-1, number));
        assertThrows(ImpossibleRollableException.class, () -> RollableUtils.getRollables(0, number));
        assertThrows(ImpossibleCupException.class, () -> RollableUtils.getRollables(faces, -1));
    }

    @Test
    void emptyRollableCup() {
        int faces = RollableTestUtils.getRandomFaces();
        Rollable[] rollables = RollableUtils.getRollables(faces, 0);

        assertEquals(0, rollables.length);
    }

    @Test
    void nDxRollableCup() {
        int faces = RollableTestUtils.getRandomFaces();
        int number = RollableTestUtils.getRandomNumber();
        Rollable[] rollables = RollableUtils.getRollables(faces, number);

        assertEquals(number, rollables.length);
        for (Rollable rollable : rollables) assertEquals(faces, rollable.getFaces());
    }

    @Test
    void impossibleCup() {
        int faces = RollableTestUtils.getRandomFaces();
        int number = RollableTestUtils.getRandomNumber();
        assertThrows(ImpossibleRollableException.class, () -> RollableUtils.getCup(-1, number));
        assertThrows(ImpossibleRollableException.class, () -> RollableUtils.getCup(0, number));
        assertThrows(ImpossibleCupException.class, () -> RollableUtils.getCup(faces, -1));
    }

    @Test
    void emptyCup() {
        int faces = RollableTestUtils.getRandomFaces();
        Cup cup = RollableUtils.getCup(faces, 0);

        assertEquals(0, cup.size());
        assertNull(cup.roll());
    }

    @Test
    void nDxCup() {
        int faces = RollableTestUtils.getRandomFaces();
        int number = RollableTestUtils.getRandomNumber();
        Cup cup = RollableUtils.getCup(faces, number);

        assertEquals(number, cup.size());
        for (Rollable rollable : cup) {
            assertNotNull(rollable);
            assertEquals(faces, rollable.getFaces());
        }
    }

}
