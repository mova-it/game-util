package mova.game.dice;

import java.util.IntSummaryStatistics;
import java.util.function.IntSupplier;
import java.util.stream.IntStream;

public class DiceStats {

    public static IntSupplier dice(int caracLevel, int retainDice, int sumMod) {
        return () -> {
            IntStream destinyDie = IntStream.of(Die.D6.roll(true));
            IntStream otherDice = IntStream.generate(Die.D6::roll).limit(caracLevel - 1);

            int result = IntStream.concat(destinyDie, otherDice)
                    .sorted()
                    .skip(caracLevel - (Integer.min(caracLevel, retainDice) + Integer.max(0, caracLevel - 5)))
                    .sum();

            return Math.max(0, result + sumMod);
        };
    }

    public static IntSummaryStatistics stats(int caracLevel, int retainDice) {
        return stats(caracLevel, retainDice, 0);
    }

    public static IntSummaryStatistics stats(int caracLevel, int retainDice, int sumMod) {
        GEDSummaryStatistics gedSummaryStatistics = new GEDSummaryStatistics();
        IntStream.generate(dice(caracLevel, retainDice, sumMod)).limit(10000).forEach(gedSummaryStatistics);
        return gedSummaryStatistics;
    }

    private static class GEDSummaryStatistics extends IntSummaryStatistics {

        private String difficulty(int r) {
            if (r < 3) return "None";
            else if (r < 6) return "Très facile";
            else if (r < 9) return "Facile";
            else if (r < 12) return "Difficile";
            else if (r < 18) return "Très difficile";
            else if (r < 25) return "Extrême";
            else return "Impossible";
        }

        @Override
        public String toString() {
            int min = getMin();
            int avg = (int) Math.round(getAverage());
            int max = getMax();
            return String.format(
                    "min=%d, average=%d, max=%d, min.difficulty=%s, avg.difficulty=%s, max.difficulty=%s",
                    min,
                    avg,
                    max,
                    difficulty(min),
                    difficulty(avg),
                    difficulty(max));
        }
    }
}
