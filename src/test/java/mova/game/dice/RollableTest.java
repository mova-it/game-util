package mova.game.dice;

import org.junit.jupiter.api.Test;

import java.util.concurrent.ThreadLocalRandom;

import static org.junit.jupiter.api.Assertions.*;

class RollableTest {

    @Test
    void impossibleDice() {
        assertThrows(IllegalArgumentException.class, () -> Die.valueOf(-1));
        assertThrows(IllegalArgumentException.class, () -> Die.valueOf(0));
        assertThrows(IllegalArgumentException.class, () -> Die.valueOf(432));
    }

    @Test
    void impossibleRollables() {
        assertThrows(ImpossibleRollableException.class, () -> Die.getRollable(-1));
        assertThrows(ImpossibleRollableException.class, () -> Die.getRollable(0));
    }

    @Test
    void d1() {
        Rollable d1 = Die.getRollable(1);
        assertThrows(InfiniteRollException.class, () -> d1.roll(true));
        assertThrows(InfiniteRollException.class, () -> d1.roll(0));
        assertEquals(1, d1.roll());
        assertEquals(1, d1.roll(0, 0));
        assertEquals(2, d1.roll(0, 1));
    }

    @Test
    void dx() {
        int faces = ThreadLocalRandom.current().nextInt(100) + 1;
        Rollable dx = Die.getRollable(faces);

        int result;

        result = dx.roll();
        assertTrue(result > 0 && result <= faces, result + " n'est pas dans l'interval [1, " + faces + "]");

        result = dx.roll(false);
        assertTrue(result > 0 && result <= faces, result + " n'est pas dans l'interval [1, " + faces + "]");

        result = dx.roll(-1);
        assertTrue(result > 0 && result <= faces, result + " n'est pas dans l'interval [1, " + faces + "]");

        assertThrows(InfiniteRollException.class, () -> dx.roll(faces - 1));
        assertThrows(InfiniteRollException.class, () -> dx.roll(faces - 1, -1));

        result = dx.roll(faces - 1, 0);
        assertTrue(result > 0 && result <= faces, result + " n'est pas dans l'interval [1, " + faces + "]");

        result = dx.roll(faces - 1, 1);
        assertTrue(result > 0 && result <= 2*faces, result + " n'est pas dans l'interval [1, 2*" + faces + "]");
    }

}
