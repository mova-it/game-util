package mova.game.dice;

import mova.util.RandomUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class ResultTableTest {

    private static final String INVALID = "invalid";
    private static final String RESULT = "result";

    private ResultTable resultTable;

    @BeforeEach
    public void setUp() {
        resultTable = new ResultTable();
    }

    @Test
    void addResult_withMinNeg() {
        assertThrows(IllegalArgumentException.class, () -> resultTable.addResult(-1, 1, INVALID));
    }

    @Test
    void addResult_withMinGreaterThanMax() {
        assertThrows(IllegalArgumentException.class, () -> resultTable.addResult(5, 2, INVALID));
    }

    @Test
    void addResult_withNullResult() {
        assertThrows(IllegalArgumentException.class, () -> resultTable.addResult(1, 1, null));
    }

    @Test
    void addResult_withEndOfRangeResult() {
        assertThrows(IllegalArgumentException.class, () -> resultTable.addResult(1, 1, "EOR"));
    }

    @Test
    void addResult_withValidRange_emptyTable() {
        addResultRandomRange(1);
    }

    @Test
    void addResults_withValidRange() {
        IntStream.rangeClosed(1, 10).forEach(this::addResultRandomRange);
    }

    private void addResultRandomRange(int i) {
        try {
            int min = RandomUtils.randInt(33);
            int gap = RandomUtils.randInt(33) + 1;
            int max = RandomUtils.randInt(33) + min + gap;
            resultTable.addResult(min+i*100, max+i*100, RESULT + i);
        } catch (Exception e) {
            Logger.getLogger(ResultTableTest.class.getName()).log(Level.SEVERE, e, () -> "Exception thrown: " + resultTable.getTable());
            fail("Exception thrown: " + e);
        }
    }

    @Test
    void addResult_withValidExactValue() {
        addResultExactValue(1);
    }

    @Test
    void addResults_withValidExactValue() {
        IntStream.rangeClosed(1, 10).forEach(this::addResultExactValue);
    }

    private void addResultExactValue(int i) {
        try {
            int x = RandomUtils.randInt(100);
            resultTable.addResult(x+i*100, RESULT + i);
        } catch (Exception e) {
            Logger.getLogger(ResultTableTest.class.getName()).log(Level.SEVERE, e, () -> "Exception thrown: " + resultTable.getTable());
            fail("Exception thrown: " + e);
        }
    }

    @Test
    void addResults_withValidRangeAndExactValue() {
        IntStream.rangeClosed(1, 10).forEach(i -> {
            if (RandomUtils.tail()) {
                addResultRandomRange(i);
            } else {
                addResultExactValue(i);
            }
        });
    }

    @Test
    void addResults_betweenRanges() {
        List<Integer> iterationIndexes = IntStream.rangeClosed(1, 10).boxed().collect(Collectors.toList());
        Collections.shuffle(iterationIndexes);

        iterationIndexes.forEach(i -> {
            if (RandomUtils.tail()) {
                addResultRandomRange(i);
            } else {
                addResultExactValue(i);
            }
        });
    }

    @Test
    void addResult_withOverlappingRange_maxOverlapping() {
        resultTable.addResult(10, 20, RESULT);
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(9, 11, INVALID));
    }

    @Test
    void addResult_withOverlappingRange_maxEqualsMin() {
        resultTable.addResult(10, 20, RESULT);
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(9, 10, INVALID));
    }

    @Test
    void addResult_withOverlappingRange_maxEqualsMax() {
        resultTable.addResult(10, 20, RESULT);
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(9, 20, INVALID));
    }

    @Test
    void addResult_withOverlappingRange_minOverlapping() {
        resultTable.addResult(10, 20, RESULT);
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(19, 21, INVALID));
    }

    @Test
    void addResult_withOverlappingRange_minEqualsMin() {
        resultTable.addResult(10, 20, RESULT);
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(10, 21, INVALID));
    }

    @Test
    void addResult_withOverlappingRange_minEqualsMax() {
        resultTable.addResult(10, 20, RESULT);
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(20, 21, INVALID));
    }

    @Test
    void addResult_withOverlappingRange_minAndMaxInRange() {
        resultTable.addResult(10, 20, RESULT);
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(11, 19, INVALID));
    }

    @Test
    void addResult_withOverlappingRange_minAndMaxOverlappingRange() {
        resultTable.addResult(10, 20, RESULT);
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(9, 21, INVALID));
    }

    @ParameterizedTest
    @ValueSource(ints = {10, 15, 20})
    void addResult_withOverlappingExactValue(int x) {
        resultTable.addResult(10, 20, RESULT);
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(x, INVALID));
    }

    @Test
    void addResult_withOverlappingRange_minEqualsExactValue() {
        resultTable.addResult(10, RESULT);
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(10, 20, INVALID));
    }

    @Test
    void addResult_withOverlappingRange_maxEqualsExactValue() {
        resultTable.addResult(20, RESULT);
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(10, 20, INVALID));
    }

    @Test
    void addResult_withExactValueEqualsExactValue() {
        resultTable.addResult(20, RESULT);
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(20, INVALID));
    }

    @Test
    void addResults_withOverlappingRange() {
        resultTable.addResult(10, 20, RESULT + "1");
        resultTable.addResult(30, RESULT + "2");
        resultTable.addResult(40, 50, RESULT + "3");
        resultTable.addResult(70, 80, RESULT + "4");

        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(11, 31, INVALID), "No Exception thrown: " + resultTable.getTable());
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(29, 41, INVALID), "No Exception thrown: " + resultTable.getTable());
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(11, 41, INVALID), "No Exception thrown: " + resultTable.getTable());
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(9, 31, INVALID), "No Exception thrown: " + resultTable.getTable());
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(9, 51, INVALID), "No Exception thrown: " + resultTable.getTable());
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(39, 71, INVALID), "No Exception thrown: " + resultTable.getTable());
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(41, 71, INVALID), "No Exception thrown: " + resultTable.getTable());
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(41, 81, INVALID), "No Exception thrown: " + resultTable.getTable());
        assertThrows(OverlappedRangeException.class, () -> resultTable.addResult(39, 81, INVALID), "No Exception thrown: " + resultTable.getTable());
    }

    @Test
    void getResult_range() {
        int min = RandomUtils.randInt(100) + 1;
        int gap = RandomUtils.randInt(100) + 1;
        int max = RandomUtils.randInt(100) + min + gap;
        resultTable.addResult(min, max, RESULT);

        assertNotEquals(RESULT, resultTable.getResult(min - 1));
        assertNotEquals(RESULT, resultTable.getResult(max + 1));
        IntStream.rangeClosed(min, max).forEach(value -> assertEquals(RESULT, resultTable.getResult(value)));
    }

    @Test
    void getResult_exactValue() {
        int x = 1 + RandomUtils.randInt(100);
        resultTable.addResult(x, RESULT);

        assertNotEquals(RESULT, resultTable.getResult(x - 1));
        assertEquals(RESULT, resultTable.getResult(x));
        assertNotEquals(RESULT, resultTable.getResult(x + 1));
    }

    @Test
    void getResult_complex() {
        resultTable.addResult(10, 20, RESULT + "1");
        resultTable.addResult(30, RESULT + "2");
        resultTable.addResult(40, 50, RESULT + "3");
        resultTable.addResult(51, 80, RESULT + "4");


        IntStream.rangeClosed(0, 9).forEach(value -> assertNull(resultTable.getResult(value)));
        IntStream.rangeClosed(10, 20).forEach(value -> assertEquals(RESULT + "1", resultTable.getResult(value)));
        IntStream.rangeClosed(21, 29).forEach(value -> assertNull(resultTable.getResult(value)));
        assertEquals(RESULT + "2", resultTable.getResult(30));
        IntStream.rangeClosed(31, 39).forEach(value -> assertNull(resultTable.getResult(value)));
        IntStream.rangeClosed(40, 50).forEach(value -> assertEquals(RESULT + "3", resultTable.getResult(value)));
        IntStream.rangeClosed(51, 80).forEach(value -> assertEquals(RESULT + "4", resultTable.getResult(value)));
        IntStream.rangeClosed(81, 100).forEach(value -> assertNull(resultTable.getResult(value)));
    }
}
