package mova.game.dice;

public class ImpossibleCupException extends IllegalArgumentException {

    public ImpossibleCupException() {
        super("Impossible cup!");
    }

    public ImpossibleCupException(int number) {
        super("Impossible cup dice number:" + number);
    }
}
