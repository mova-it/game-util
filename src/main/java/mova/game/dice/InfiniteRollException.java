package mova.game.dice;

public class InfiniteRollException extends IllegalArgumentException {

    public InfiniteRollException(int faces, int marge, int maxReroll) {
        super("faces = " + faces + "; marge = " + marge + "; maxReroll = " + maxReroll);
    }
}
