package mova.game.dice;

import java.util.Arrays;

class RollableUtils {

    @SuppressWarnings("unused")
    private RollableUtils() {}

    static Rollable[] getRollables(int faces, int number) {
        return getCup(faces, number).getContent();
    }

    static Cup getCup(int faces, int number) {
        Rollable rollable = Die.getRollable(faces);

        if (number < 0) throw new ImpossibleCupException(number);
        Rollable[] rollables = new Rollable[number];
        Arrays.fill(rollables, rollable);

        return new Cup(rollables);
    }

    static Cup.Roll roll(Rollable... rollables) {
        return roll(0, -1, rollables);
    }

    static Cup.Roll roll(int openRollablesNumber, Rollable... rollable) {
        return roll(openRollablesNumber, 0, rollable);
    }

    static Cup.Roll roll(int openRollablesNumber, int marge, Rollable... rollables) {
        Cup cup = new Cup(rollables);
        return roll(cup, openRollablesNumber, marge);
    }

    static Cup.Roll roll(int[] marges, Rollable... rollables) {
        Cup cup = new Cup(rollables);
        return roll(cup, marges);
    }

    static Cup.Roll roll(Cup cup) {
        return roll(cup, 0, -1);
    }

    static Cup.Roll roll(Cup cup, int openRollablesNumber) {
        return roll(cup, openRollablesNumber, 0);
    }

    static Cup.Roll roll(Cup cup, int openRollablesNumber, int marge) {
        if (openRollablesNumber < 0) throw new IllegalArgumentException("Wrong open rollables number: " + openRollablesNumber);

        int[] marges = new int[openRollablesNumber];
        Arrays.fill(marges, marge);

        return roll(cup, marges);
    }

    static Cup.Roll roll(Cup cup, int[] marges) {
        return cup.roll((rollable, i) -> i < marges.length ? rollable.roll(marges[i]) : rollable.roll() );
    }

    static Cup.Roll roll(Rollable rollable, int number) {
        return roll(rollable, number, 0, -1);
    }

    static Cup.Roll roll(Rollable rollable, int number, int openRollablesNumber) {
        return roll(rollable, number, openRollablesNumber, 0);
    }

    static Cup.Roll roll(Rollable rollable, int number, int openRollablesNumber, int marge) {
        if (number < 0) throw new IllegalArgumentException("Wrong rollables number: " + number);

        Rollable[] rollables = new Rollable[number];
        Arrays.fill(rollables, rollable);

        return roll(openRollablesNumber, marge, rollables);
    }

    static Cup.Roll roll(Rollable rollable, int number, int[] marges) {
        if (number < 0) throw new IllegalArgumentException("Wrong rollables number: " + number);

        Rollable[] rollables = new Rollable[number];
        Arrays.fill(rollables, rollable);

        return roll(marges, rollables);
    }

    static int interpret(String formula) {
        return interpret(new DiceFormulaInterpretor(formula));
    }

    static int interpret(DiceFormulaInterpretor interpretor) {
        return interpretor.interpret();
    }
}
