package mova.game.dice;

/**
 * Une énumeration de Des que l'on peut lancer. On peut également effectuer des
 * lancés ouverts et préciser la marge d'ouverture de ce lancer.<br/>
 * Par exemple, un lancer ouvert d'un D6 avec une marge de 0, consiste à lancer
 * un D6 et si le résultat est 6 (6 - 0) on relance le dé et on ajoute le
 * résultat. On relance à chaque 6.
 * Un autre exemple avec un D100 et une marge de 5 ; dans ce cas on relance le
 * dé tant que le résultat est dans l'intervalle ]95, 100].
 *
 * @author Mohicane
 */
public enum Die implements Rollable {

    /** Un dé à 2 faces ou une pièce. */
    D2,
    /**
     * Un dé à 3 faces (en général ces dés sont simulés par le lancer d'un dé à
     * 6 faces et sur 1 ou 2, on compte 1, sur 3 ou 4, on compte 2 et sur 5 ou 6
     * on compte 3).
     */
    D3,
    /** Un dé à 4 faces. */
    D4,
    /** Un dé à 6 faces. */
    D6,
    /** Un dé à 8 faces. */
    D8,
    /** Un dé à 10 faces. */
    D10,
    /** Un dé à 12 faces. */
    D12,
    /** Un dé à 20 faces. */
    D20,
    /** Un dé à 100 faces. */
    D100;

    /** Le préfixe du nom d'un dé dans l'énumération. */
    static final String DIE_NAME_PREFIX = "D";

    /** Nombre de faces du dé. */
    private final int faces;

    /**
     * Construit un nouveau dé et initialise le nombre de ses faces en fonction
     * de son nom suivant le shéma suivant DEX ou X est le nombre de faces.
     */
    Die() {
        faces = Integer.parseInt(name().substring(DIE_NAME_PREFIX.length()));
    }

    @Override
    public int getFaces() {
        return faces;
    }

    public static Die valueOf(int faces) {
        return Die.valueOf(Die.DIE_NAME_PREFIX + faces);
    }

    public static Rollable getRollable(int faces) {
        try {
            return Die.valueOf(faces);
        } catch (IllegalArgumentException iae) {
            if (faces <= 0) throw new ImpossibleRollableException(faces);

            return () -> faces;
        }
    }
}
