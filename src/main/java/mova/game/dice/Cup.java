package mova.game.dice;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.IntUnaryOperator;
import java.util.function.ToIntBiFunction;
import java.util.function.ToIntFunction;

class Cup implements Iterable<Rollable> {

    private final Rollable[] rollables;

    Cup(Rollable[] rollables) {
        if (rollables == null) throw new ImpossibleCupException();
        if (ArrayUtils.contains(rollables, null)) throw new ImpossibleCupException();

        this.rollables = rollables;
    }

    int size() {
        return rollables.length;
    }

    Rollable[] getContent() {
        return rollables;
    }

    Roll roll() {
        return roll((ToIntFunction<Rollable>) Rollable::roll);
    }

    Roll roll(ToIntFunction<Rollable> rollableFunction) {
        return roll0(i -> rollableFunction.applyAsInt(rollables[i]));
    }

    Roll roll(ToIntBiFunction<Rollable, Integer> rollableBiFunction) {
        return roll0(i -> rollableBiFunction.applyAsInt(rollables[i], i));
    }

    private Roll roll0(IntUnaryOperator operator) {
        if (rollables.length == 0) return null;

        int[] results = new int[rollables.length];

        Arrays.setAll(results, operator);

        return new Roll(results);
    }

    @Override
    public String toString() {
        return Arrays.toString(rollables);
    }

    @Override
    public Iterator<Rollable> iterator() {
        return new Iterator<Rollable>() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < rollables.length;
            }

            @Override
            public Rollable next() {
                if (hasNext()) return rollables[index++];

                throw new NoSuchElementException();
            }
        };
    }

    static class Roll implements Iterable<Integer> {

        private int[] results;

        Roll(int[] results) {
            this.results = results;
        }

        Roll(Roll roll) {
            this(roll.results);
        }

        int[] getResults() {
            return results;
        }

        int size() {
            return results.length;
        }

        Roll retains(int resultsNumber) {
            return retains(resultsNumber, RetainMode.HIGHEST);
        }

        Roll retains(int resultsNumber, RetainMode mode) {
            if (resultsNumber < 0 || resultsNumber > results.length) {
                throw new IllegalArgumentException(resultsNumber + " for " + results.length);
            }

            Arrays.sort(results);

            if (RetainMode.HIGHEST.equals(mode)) {
                results = Arrays.copyOfRange(results, results.length - resultsNumber, results.length);
            } else {
                results = Arrays.copyOfRange(results, 0, resultsNumber);
            }

            return this;
        }

        int sum() {
            return Arrays.stream(results).sum();
        }

        @Override
        public String toString() {
            return Arrays.toString(results);
        }

        @Override
        public Iterator<Integer> iterator() {
            return new Iterator<Integer>() {
                private int index = 0;

                @Override
                public boolean hasNext() {
                    return index < results.length;
                }

                @Override
                public Integer next() {
                    if (hasNext()) return results[index++];

                    throw new NoSuchElementException();
                }
            };
        }

        enum RetainMode {
            HIGHEST,
            LOWEST
        }
    }
}
