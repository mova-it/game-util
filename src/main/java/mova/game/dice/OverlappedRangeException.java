package mova.game.dice;

class OverlappedRangeException extends RuntimeException {

    OverlappedRangeException(int min, int max) {
        super(String.format("Les plages de valeurs ne doivent pas se chevaucher: [%d, %d]", min, max));
    }
}
