package mova.game.dice;

import java.util.*;

public class ResultTable {

    private static final String END_OF_RANGE = "EOR";
    private final TreeMap<Integer, String> table = new TreeMap<>();

    public void addResult(int x, String result) {
        addResult(x, x, result);
    }

    public void addResult(int min, int max, String result) {
        if (min < 0) {
            throw new IllegalArgumentException("La borne inférieure doit être positive");
        }

        if (min > max) {
            throw new IllegalArgumentException("La borne inférieure doit être inférieure ou égale à la borne supérieure");
        }

        if (result == null || result.equals(END_OF_RANGE)) {
            throw new IllegalArgumentException("Un résultat ne peut être null ou égal à EOR (mot clé réservé)");
        }

        if (isRangeBusy(min, max)) throw new OverlappedRangeException(min, max);

        table.put(min, result);
        if (max != min) table.put(max, END_OF_RANGE);
    }

    private boolean isRangeBusy(int min, int max) {
        // Si la table est vide ou si la plage de valeurs est en dehors de la table, on renvoie false
        if (table.isEmpty() || min > table.lastKey() || max < table.firstKey()) {
            return false;
        }

        // Si les bornes min et max sont déjà dans la table, on renvoie true
        if (table.get(min) != null || table.get(max) != null) {
            return true;
        }

        // La valeur associée à min ne peut être suivi de END_OF_RANGE
        if (table.ceilingEntry(min).getValue().equals(END_OF_RANGE)) {
            return true;
        }

        // S'il s'agit d'une valeur exact, pas besoin de controler le chevauchement
        if (min == max) {
            return false;
        }

        // Si la plage de valeurs est déjà occupée, on renvoie true
        return !table.subMap(min, true, max, true).isEmpty();
    }

    public String getResult(int x) {
        if (x < 0) throw new IndexOutOfBoundsException(Integer.toString(x));

        Map.Entry<Integer, String> floorEntry = table.floorEntry(x);
        // Si la clé est inférieure à la première clé de la table, on renvoie null
        if (floorEntry == null) return null;

        if (floorEntry.getKey() == x) {
            // Si x vaut une borne de la table, on renvoie la valeur associée
            // Si la valeur associée est EOR (END_OF_RANGE), la valeur associée est celle de l'entrée précédente
            return END_OF_RANGE.equals(floorEntry.getValue()) ? table.lowerEntry(x).getValue() : floorEntry.getValue();
        }
        else {
            // On vérifie qu'on est entre deux bornes de la table, c'est à dire que la valeur associée à la clé suivante est EOR
            Map.Entry<Integer, String> ceilingEntry = table.ceilingEntry(x);
            if (ceilingEntry != null && END_OF_RANGE.equals(ceilingEntry.getValue())) {
                return floorEntry.getValue();
            }
        }

        // Si on ne trouve pas de valeur (on est donc hors bornes), on renvoie null
        return null;
    }

    protected TreeMap<Integer, String> getTable() {
        return table;
    }

}
