package mova.game.dice;

import java.util.concurrent.ThreadLocalRandom;

public interface Rollable {

    int getFaces();

    @SuppressWarnings("java:S2245")
    default int roll() {
        return ThreadLocalRandom.current().nextInt(getFaces()) + 1;
    }

    default int roll(boolean ouvert) {
        return ouvert ? roll(0):  roll();
    }

    default int roll(int marge)  {
        return roll(marge, -1);
    }

    default int roll(int marge, int maxReroll) {
        int rethrowAt = getFaces() - marge;

        if (maxReroll < 0 && rethrowAt <= 1) {
            // Si la rethrowAt est inférieur ou égale à 1 et qu'on ne limite pas le nombre d'iteration, la boucle est infinie.
            throw new InfiniteRollException(getFaces(), marge, maxReroll);
        }

        int result = 0;
        int roll;
        do {
            roll = roll();
            result += roll;
        } while (roll >= rethrowAt && (maxReroll == -1 || maxReroll-- > 0));

        return result;
    }
}
