package mova.game.dice;

import mova.lang.MathUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

class DiceFormulaInterpretor {

    private static final String NUMBER_REGEX = "([1-9]\\d*)";
    private static final String OPERATOR_REGEX = "([" + MathUtils.PLUS + "\\" + MathUtils.MINUS + "])";
    private static final String MARGE_REGEX = "\\[" + NUMBER_REGEX + "]";
    private static final String REITERATION_REGEX = "\\{" + NUMBER_REGEX + "}";
    private static final String UNSIGNED_TERM_REGEX = "((?:" + NUMBER_REGEX + "?" + Die.DIE_NAME_PREFIX + ")?" + NUMBER_REGEX + "(?:" + MARGE_REGEX + "(?:" + REITERATION_REGEX + ")?)?|0)";
    private static final Pattern SIGNED_TERM_PATTERN = Pattern.compile(OPERATOR_REGEX + "?" + UNSIGNED_TERM_REGEX);

    private static final String FORMULE_REGEX = "(" + OPERATOR_REGEX + "?" + UNSIGNED_TERM_REGEX + ")" + "(" + OPERATOR_REGEX + UNSIGNED_TERM_REGEX + ")*";
    private static final Pattern FORMULE_PATTERN = Pattern.compile(FORMULE_REGEX);

    private final String formula;

    DiceFormulaInterpretor(String formula) {
        this.formula = StringUtils.deleteWhitespace(formula);

        if (!FORMULE_PATTERN.matcher(this.formula).matches()) {
            throw new IllegalArgumentException("\"" + this.formula + "\" doesn't respect \"" + FORMULE_PATTERN.pattern() + "\"");
        }
    }

    int interpret() {
        int resultat = 0;

        TermIterator iterator = new TermIterator();
        while (iterator.hasNext()) {
            Term term = iterator.nextTerm();
            resultat += term.getResult();
        }
        return resultat;
    }

    static int resolve(String stringFormule) {
        return new DiceFormulaInterpretor(stringFormule).interpret();
    }

    private class TermIterator implements Iterator<String> {

        static final int WHOLE_TERM_GROUP_INDEX = 0;
        static final int SIGNUS_GROUP_INDEX = 1;
        static final int NUMBER_GROUP_INDEX = 2;
        static final int FACTOR_GROUP_INDEX = 3;
        static final int FACES_GROUP_INDEX = 4;
        static final int MARGE_GROUP_INDEX = 5;
        static final int REITERATION_GROUP_INDEX = 6;

        private final Matcher matcher;

        TermIterator() {
            matcher = SIGNED_TERM_PATTERN.matcher(formula);
        }

        public boolean hasNext() {
            try {
                return matcher.end() < formula.length();
            } catch (IllegalStateException e) {
                return !matcher.hitEnd();
            }
        }

        public String next() {
            if (!matcher.find()) throw new NoSuchElementException("No match found");
            return matcher.group();
        }

        private String[] nextGroups() {
            if (!matcher.find()) throw new NoSuchElementException("No match found");
            return IntStream.rangeClosed(0, matcher.groupCount()).mapToObj(matcher::group).toArray(String[]::new);
        }

        private Term nextTerm() {
            String[] groups = nextGroups();

            String term = groups[WHOLE_TERM_GROUP_INDEX];
            if (isDiceTerm(term)) return new DiceTerm(groups);
            else return new NumberTerm(groups[SIGNUS_GROUP_INDEX], groups[NUMBER_GROUP_INDEX]);
        }

        private boolean isDiceTerm(String term) {
            return term.contains(Die.DIE_NAME_PREFIX);
        }

    }

    interface Term {
        int POSITIVE_MOD = 1;
        int NEGATIVE_MOD = -1;

        int getResult();

        static int getSignusMod(String signus) {
            return Objects.equals(MathUtils.MINUS, signus) ? NEGATIVE_MOD : POSITIVE_MOD;
        }
    }

    private static class NumberTerm implements Term {

        final int result;

        NumberTerm(String signus, String number) {
            this(Term.getSignusMod(signus), number);
        }

        private NumberTerm(int mod, String number) {
            result = mod*Integer.parseInt(number);
        }

        @Override
        public int getResult() {
            return result;
        }
    }

    private static class DiceTerm implements Term {

        private static final int DEFAULT_NUMBER = 1;
        private static final int DEFAULT_MARGE = -1;
        private static final int DEFAULT_REITERATION = -1;

        final int result;

        DiceTerm(String[] groups) {
            String signus = groups[TermIterator.SIGNUS_GROUP_INDEX];
            int mod = Term.getSignusMod(signus);
            int number = parse(groups[TermIterator.FACTOR_GROUP_INDEX], DEFAULT_NUMBER);
            int faces = Integer.parseInt(groups[TermIterator.FACES_GROUP_INDEX]);
            int marge = parse(groups[TermIterator.MARGE_GROUP_INDEX], DEFAULT_MARGE);
            int reiteration = parse(groups[TermIterator.REITERATION_GROUP_INDEX], DEFAULT_REITERATION);


            result = RollableUtils.getCup(faces, number).roll(die -> die.roll(marge, reiteration)).sum()*mod;
        }

        @Override
        public int getResult() {
            return result;
        }

        private int parse(String s, int other) {
            return s != null ? Integer.parseInt(s) : other;
        }
    }
}
