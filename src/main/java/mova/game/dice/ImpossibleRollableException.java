package mova.game.dice;

public class ImpossibleRollableException extends IllegalArgumentException {

    public ImpossibleRollableException(int faces) {
        super("Impossible die faces:" + faces);
    }
}
